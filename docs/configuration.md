# Configuration

Out of the box, hcli has sane defaults, and [plenty of options](../config/hcli.json) to configure it.

Put only the options to override in a configuration file. Here is a list of file names where hcli looks for
configuration in the root of the project:

- `.hcli.json`
- `.hcli.js` (or `.cjs`; export the configuration object: `module.exports = {}`)
- `.hcli.yaml` (or `.yml`)
- `.hcli.toml`
- `package.json` (in the `hcli` property)

Use `--config path/hcli.json` to use another configuration file location.

An example `.hcli.json`:

```json
{
  "git": {
    "commitMessage": "chore: release v${version}"
  },
  "github": {
    "release": true
  }
}
```

The configuration can also be stored in a `hcli` property in `package.json`:

```json
{
  "name": "my-package",
  "devDependencies": {
    "hcli": "*"
  },
  "hcli": {
    "github": {
      "release": true
    }
  }
}
```

Or, use YAML in `.hcli.yml`:

```yaml
git:
  requireCleanWorkingDir: false
```

TOML is also supported in `.hcli.toml`:

```toml
[hooks]
"before:init" = "npm test"
```

Any option can also be set on the command-line, and will have highest priority. Example:

```bash
hcli minor --git.requireBranch=master --github.release
```

Boolean arguments can be negated by using the `no-` prefix:

```bash
hcli --no-npm.publish
```

Also plugin options can be set from the command line:

```bash
hcli --no-plugins.@hcli/keep-a-changelog.strictLatest
```

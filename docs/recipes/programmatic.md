# Use hcli programmatically

From Node.js scripts, hcli can also be used as a dependency:

```js
import release from 'hcli';

release(options).then(output => {
  console.log(output);
  // { version, latestVersion, name, changelog }
});
```
